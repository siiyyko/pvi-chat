const express = require('express');
const {markChatAsRead, addUnreadNotification, getUnreadNotifications} = require('../controllers/notificationController')

const router = express.Router();

router.post("/readChat", markChatAsRead)
router.post("/addNotif", addUnreadNotification)
router.get("/:idUnreadForWho", getUnreadNotifications)

module.exports = router;