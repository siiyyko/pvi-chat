const express = require('express');
const cors = require('cors');
const mongoose = require('mongoose');
const userRoute = require('./routes/userRoute');
const chatRoute = require('./routes/chatRoute');
const messageRoute = require('./routes/messageRoute')
const notificationRoute = require('./routes/notificationRoute');

const app = express();
require('dotenv').config();

app.use(express.json());
app.use(cors());
app.use('/api/users', userRoute);
app.use('/api/chats', chatRoute);
app.use('/api/messages', messageRoute)
app.use('/api/notification', notificationRoute);

//CRUD
//C: create, POST
//R: read, GET
//U: update, PUT
//D: delete, DELETE

app.get('/', (req, res) => {
    res.send('Hi');
})

const port = 5000;
const uri = process.env.ATLAS_URI;

app.listen(port, (req, res) => {
    console.log(`Server running in port ${port}`);
});

mongoose.connect(uri)
.then(() => {
    console.log('Database connected!');
})
.catch((error) =>{
    console.log("Connection failed:( Error: ", error.message);
});