const mongoose = require("mongoose");

const notificationSchema = new mongoose.Schema(
    {
      chatId: String,
      idUnreadForWho: String,
      unreadCount: Number,
    },
    {
      timestamps: true,
    }
  );

const notificationModel = mongoose.model("Notification", notificationSchema);

module.exports = notificationModel;