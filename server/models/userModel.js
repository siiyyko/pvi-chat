const mongoose = require('mongoose');

//schema for a database
const userSchema = new mongoose.Schema({
    name: {type: String, required: true, unique: true},
    password: {type: String}
},{
    timestamps: true,
});

const userModel = mongoose.model('User', userSchema);

module.exports = userModel;