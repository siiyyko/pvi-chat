const notificationModel = require("../models/notificationModel");

const markChatAsRead = async (req, res) => {
  const { chatId, idUnreadForWho } = req.body;

  try {
    const response = await notificationModel.findOneAndUpdate(
      { chatId, idUnreadForWho },
      { unreadCount: 0 },
      { new: true }
    );

    return res.status(200).json(response);
  } catch (error) {
    console.log(error);
    res.status(500).json(error);
  }
};

const addUnreadNotification = async (req, res) => {
  const { chatId, idUnreadForWho } = req.body;
  try {
    let incrementBy = 1;
    const response = await notificationModel.findOneAndUpdate(
      { chatId, idUnreadForWho },
      { $inc: { unreadCount: incrementBy } },
      { new: true, upsert: true }
    );
    return res.status(200).json(response);
  } catch (error) {
    console.error(error);
    return res.status(500).json(error);
  }
};

const getUnreadNotifications = async (req, res) => {
  const {idUnreadForWho} = req.params;
  console.log(`params: ${idUnreadForWho}`);

  try {
    const notifications = await notificationModel.find({ idUnreadForWho });

    const result = notifications.map((notification) => ({
      chatId: notification.chatId,
      unreadCount: notification.unreadCount,
    }));

    return res.status(200).json(result);
  } catch (error) {
    console.log(error);
    return res.status(500).json(error);
  }
};

module.exports = {markChatAsRead, addUnreadNotification, getUnreadNotifications}
