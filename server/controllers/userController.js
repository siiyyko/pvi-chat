const userModel = require('../models/userModel');
const bcrypt = require('bcrypt');
const validator = require('validator');
const jwt = require('jsonwebtoken');

const createToken = (_id) => {
    const jwtkey = process.env.JWT_SECRET_KEY;
    return jwt.sign({_id}, jwtkey, {expiresIn: "3d"});
}

const signupUser = async (req, res) => {
    try{
        const {name, password} = req.body;

        let user = await userModel.findOne({name});

        if(user) return res.status(400).json('This username already exists');

        if(!name || !password) return res.status(400).json('All fields are required');

        user = new userModel({name, password});

        //time for password hashing!
        const saltRounds = 10; // how many times (2^10) to apply hashing function
        const hashedPassword = await bcrypt.hash(user.password, saltRounds);
        user.password = hashedPassword;
        
        await user.save();

        const token = createToken(user._id);

        res.status(200).json({_id: user._id, name, token});
    } catch(error){
        console.log(error);
        res.status(500).json(error);
    };
}

const loginUser = async(req, res) => {
    const {name, password} = req.body;

    try{
        let user = await userModel.findOne({name});
        if(!user) return res.status(400).json("Invalid username or password");

        const isValidPassword = await bcrypt.compare(password, user.password);
        if(!isValidPassword) return res.status(400).json("Invalid username or password");

        const token = createToken(user._id);

        res.status(200).json({_id: user._id, name, token});
    }
    catch(error){
        console.log(error);
        res.status(500).json(error);
    }
}

const findUser = async(req, res) => {
    const userId = req.params.userId;
    try{
        const user = await userModel.findById(userId);

        res.status(200).json(user);
    }
    catch{
        console.log(error);
        res.status(500).json(error);
    }
}

const getUsers = async(req, res) => {
    try{
        const users = await userModel.find();

        res.status(200).json(users);
    }
    catch{
        console.log(error);
        res.status(500).json(error);
    }
}

module.exports = {signupUser, loginUser, findUser, getUsers};