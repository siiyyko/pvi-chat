const { Server } = require("socket.io");

const io = new Server({ cors: "http://localhost:5173" });

io.on('connection', (socket) => {
    console.log('A user connected:', socket.id);
  
    socket.on('joinRoom', ({ roomId, userId }) => {
      socket.join(roomId);
      console.log(`User ${userId} joined room ${roomId}`);
    });
  
    socket.on('sendMessage', ({ roomId, userId, textMessage }) => {
        console.log("MESSAGE: ", textMessage)
        io.to(roomId).emit('receiveMessage', {textMessage});
    });
  
    socket.on('disconnect', () => {
      console.log('A user disconnected:', socket.id);
    });
  });
  

const port = 3500;

io.listen(port, () => {
    console.log("Server is listening on port ", port);
});