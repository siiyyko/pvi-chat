import { Container, Nav, Navbar, NavDropdown, Stack } from "react-bootstrap";
import { Link } from "react-router-dom";
import "../assets/navbar.css";
import { useContext } from "react";
import { AuthContext } from "../context/AuthContext";
import Notification from "../components/chat/Notification"

const NavBar = () => {
  const { user, logoutUser } = useContext(AuthContext);
  return (
    <Navbar className="mb-4 mainContainer">
      <Container>
        <Link className="homeLink" to="/">
          CMS
        </Link>
        <h2></h2>
        <Nav className="align-items-center">
          {user && (
            <>
              <Notification/>
              <NavDropdown title={user?.name} id="basic-nav-dropdown">
                <NavDropdown.Item href="#action/3.1">
                  My profile
                </NavDropdown.Item>
                <NavDropdown.Item onClick={() => logoutUser()} href="/login">
                  Log out
                </NavDropdown.Item>
              </NavDropdown>
            </>
          )}
          {!user && (
            <>
              <Stack direction="horizontal" gap={3}>
                <Link className="NavBarLink" to="/login">
                  Login
                </Link>
                <Link className="NavBarLink" to="/signup">
                  Sign up
                </Link>
              </Stack>
            </>
          )}
        </Nav>
      </Container>
    </Navbar>
  );
};

export default NavBar;
