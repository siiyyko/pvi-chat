import { createContext, useCallback, useEffect, useState } from "react";
import { baseUrl, postRequest } from "../utils/services";
import { useNavigate } from "react-router-dom";
export const AuthContext = createContext();

export const AuthContextProvider = ({ children }) => {
  const [user, setUser] = useState(null);
  const [signupError, setSignupError] = useState(null);
  const [isSignupLoading, setSignupLoading] = useState(false);
  const [signupInfo, setSignupInfo] = useState({
    name: "",
    password: "",
  });
  const [loginError, setLoginError] = useState(null);
  const [isLoginLoading, setLoginLoading] = useState(false);
  const [loginInfo, setLoginInfo] = useState({
    name: "",
    password: "",
  })

  useEffect(() => {
    const user = localStorage.getItem("User");
    if(user) setUser(JSON.parse(user));
  }, [])

  const updateSignupInfo = useCallback((info) => {
    setSignupInfo(info);
  }, []);
  const signupUser = useCallback(async(e) => {
    e.preventDefault();
    setSignupLoading(true);
    setSignupError(null);

    const response = await postRequest(`${baseUrl}/users/signup`, JSON.stringify(signupInfo));

    setSignupLoading(false);

    if(response.error){
      return setSignupError(response);
    }

    localStorage.setItem("User", JSON.stringify(response));
    setUser(response);
  }, [signupInfo])

  const updateLoginInfo = useCallback((info) => {
    setLoginInfo(info);
  }, []);


  const loginUser = useCallback(async(e) => {
    e.preventDefault();

    setLoginLoading(true);
    setLoginError(null);

    const response = await postRequest(`${baseUrl}/users/login`, JSON.stringify(loginInfo));

    setLoginLoading(false);
    if(response.error){
      return setLoginError(response);
    }

    localStorage.setItem("User", JSON.stringify(response));
    setUser(response);

  }, [loginInfo])

  const navigate = useNavigate();
  const logoutUser = useCallback(() => {
    localStorage.removeItem("User");
    setUser(null);
    navigate('/login');
  }, [])

  return (
    <AuthContext.Provider
        value={{
            user,
            signupInfo,
            updateSignupInfo,
            signupUser,
            signupError,
            isSignupLoading,
            logoutUser,
            loginUser,
            loginError,
            loginInfo,
            updateLoginInfo,
            isLoginLoading
        }
        }>{children}
    </AuthContext.Provider>
  );
};
