import { Stack } from "react-bootstrap";
import { useContext, useEffect, useRef, useState } from "react";
import { getRequest, postRequest } from "../utils/services";
import { baseUrl } from "../utils/services";
import { AuthContext } from "../context/AuthContext";
import { ChatContext } from "../context/ChatContext";
import { useFetchRecipientUser } from "../hooks/useFetchRecipient";
import InputEmoji from "react-input-emoji";
import moment from "moment";

import { io } from "socket.io-client";

const ChatRoom = () => {
  const { user } = useContext(AuthContext);
  const userId = user._id;
  const roomId = 1;

  // const {currentChat, messages, isMessagesLoading, sendTextMessage } = useContext(ChatContext);
  //const {recipientUser} = useFetchRecipientUser(currentChat, user);
  const [textMessage, setTextMessage] = useState("");
  const [chatRoomUsers, setChatRoomUsers] = useState([]);
  const [socket, setSocket] = useState(null);
  const [messages, setMessages] = useState([]);
  const [allUsers, setAllUsers] = useState([]);
  const [currentRoom, setCurrentRoom] = useState(null);

  const scroll = useRef();

  useEffect(() => {
    scroll.current?.scrollIntoView({behavior: "smooth"});
}, [messages])

    useEffect(() => {
        setCurrentRoom(roomId);
    }, []);

  useEffect(() => {
    const getAllUsers = async () => {
      const response = await getRequest(`${baseUrl}/users`);
      setAllUsers(response);
    };

    getAllUsers();
  }, [user]);

  //create socket
  useEffect(() => {
    const newSocket = io("http://localhost:3000");
    setSocket(newSocket);
    console.log("Connected with socket: ", newSocket);

    return () => {
      newSocket.disconnect();
    };
  }, [user]);

  //connect to room
  useEffect(() => {
    if (socket === null) return;
    socket.emit("joinRoom", { roomId, userId });

    socket.on("getChatRoomUsers", (res) => {
      setChatRoomUsers(res);
    });

    return () => {
      socket.disconnect();
    };
  }, [socket]);

  //receive message
  useEffect(() => {
    if (socket === null) return;
    socket.on("receiveMessage", (textMessage) => {
      console.log(textMessage);
      setMessages((prev) => [...prev, textMessage]);
    });
  }, [socket]);

  const sendMessage = async () => {
    if (!textMessage) return;

    const response = await postRequest(
      `${baseUrl}/messages`,
      JSON.stringify({
        chatId: roomId,
        senderId: userId,
        text: textMessage,
      })
    );
    if(response.error){
        console.log("response", response);
    }

    socket.emit("sendChatRoomMessage", { roomId, textMessage, userId });
    setTextMessage("");
  };

useEffect(() => {
    const getMessages = async () => {
        const response = await getRequest(
            `${baseUrl}/messages/${roomId}`
        );
        
        const transformedResponse = response.map(m => ({
            roomId: m.chatId,
            textMessage: m.text,
            userId: m.senderId
        }))
        setMessages(transformedResponse);
    }
    getMessages();
}, [currentRoom])

  return (
    <>
      <Stack gap={4} className="chat-box" style={{ minHeight: "80vh" }}>
        <div className="chat-header">
          <strong>PZ-22 CHAT</strong>
        </div>
        <Stack gap={3} className="messages">
          {messages &&
            messages.map((message, index) => (
              <Stack
                key={index}
                className={`${
                  message?.userId === user?._id
                    ? "message self align-self-end flex-grow-0"
                    : "message align-self-start flex-grow-0"
                }`}
                ref={scroll}
              >
                {message?.userId !== user?._id ? (
                  <span className="message-username">
                    {allUsers.find((u) => u._id === message.userId).name}
                  </span>
                ) : (
                  ""
                )}
                
                <span className="message-text">{message.textMessage}</span>
                <span className="message-footer">
                  {moment(message.createdAt).calendar()}
                </span>
              </Stack>
            ))}
        </Stack>
        <Stack
          direction="horizontal"
          gap={3}
          className="chat-input flex-grow-0"
        >
          <InputEmoji value={textMessage} onChange={setTextMessage} />
          <button className="send-btn" onClick={() => sendMessage()}>
            Send!
          </button>
        </Stack>
      </Stack>
    </>
  );
};

export default ChatRoom;
