import { Alert, Button, Form, Row, Col, Stack } from "react-bootstrap";
import "../assets/signup.css";
import { useContext } from "react";
import { AuthContext } from "../context/AuthContext";

const Login = () => {
  const { loginUser, loginError, loginInfo, updateLoginInfo, isLoginLoading } =
    useContext(AuthContext);

  return (
    <>
      <Form onSubmit={loginUser}>
        {loginError?.error && (
          <Alert
            variant="danger"
            style={{
              marginTop: "2%",
              height: "10vh",
              justifyContent: "center",
              display: "flex",
            }}
          >
            <p>{loginError?.message}</p>
          </Alert>
        )}

        <Row
          style={{
            justifyContent: "center",
            marginTop: "1%",
          }}
        >
          <Col
            xs="4"
            style={{
              padding: "20px",
              border: "3px solid rgb(249, 243, 255)",
              borderRadius: "5px",
              backgroundImage:
                "linear-gradient(to top, rgb(247, 192, 247), rgb(249, 243, 255))",
            }}
          >
            <Stack gap={3}>
              <h2>Log in!</h2>
              <h5>Name</h5>
              <Form.Control
                type="text"
                placeholder="Enter your name"
                onChange={(e) => {
                  updateLoginInfo({ ...loginInfo, name: e.target.value });
                }}
              />
              <h5>Password</h5>
              <Form.Control
                type="password"
                placeholder="Enter your password"
                onChange={(e) => {
                  updateLoginInfo({ ...loginInfo, password: e.target.value });
                }}
              />
              <Button className="formButton" type="submit">
                {isLoginLoading? "Loading..." : "Log in!"}
              </Button>
            </Stack>
          </Col>
        </Row>
      </Form>
    </>
  );
};

export default Login;
