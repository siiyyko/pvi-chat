import { useContext } from "react";
import { ChatContext } from "../context/ChatContext";
import { Container, Row, Col, Stack } from "react-bootstrap";
import "../assets/chats.css";
import UserChat from "../components/chat/UserChat";
import PotentialChats from "../components/chat/PotentialChats";
import { AuthContext } from "../context/AuthContext";
import ChatBox from "../components/chat/ChatBox";
import ChatRoom from "../components/chat/ChatRoom";
import { Link } from "react-router-dom";

const Chat = () => {
  const { userChats, isUserChatsLoading, updateCurrentChat } = useContext(ChatContext);
  const { user } = useContext(AuthContext);

  return (
    <Container>
        <PotentialChats/>
        {userChats?.length < 1 ? null : (
            <Stack direction="horizontal" gap={1} className="align-items-start">
                <Stack className="flex-grow-0 pe-3 allChats" gap={3}>
                    <Link to="/chatroom" style={{color: "black", textDecoration: "none"}}>
                        <ChatRoom/>
                    </Link>
                    {isUserChatsLoading && <p>Loading chats!</p>}
                    {userChats?.map((chat, index) => {
                        return(
                            <div key={index} onClick={() => updateCurrentChat(chat)}>
                                <UserChat chat={chat} user={user}/>
                            </div>
                        )
                    })}
                </Stack>
                <ChatBox/>
            </Stack>
        )}
    </Container>
  );
};

export default Chat;
