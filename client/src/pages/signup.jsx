import { useContext } from "react";
import { Alert, Button, Form, Row, Col, Stack } from "react-bootstrap";
import "../assets/signup.css";
import { AuthContext } from "../context/AuthContext";

const Signup = () => {
    const {signupInfo, updateSignupInfo, signupUser, signupError, isSignupLoading} = useContext(AuthContext)
  return (
    <>
      <Form onSubmit={signupUser}>
        {signupError?.error && <Alert variant="danger" style={
        {
            marginTop: "2%",
            height: "10vh",
            justifyContent: "center",
            display: "flex",
        }
      }>
            <p>{signupError?.message}</p>
        </Alert>}
      
        <Row style={
            {
                justifyContent: "center",
                marginTop: "1%"
            }
        }>
            
          <Col xs="4" style={
            {
                padding: "20px",
                border: "3px solid rgb(249, 243, 255)",
                borderRadius: "5px",
                backgroundImage: "linear-gradient(to top, rgb(247, 192, 247), rgb(249, 243, 255))"
            }
          }>
            <Stack gap={3}>
                <h2>Sign up!</h2>
                <h5>Name</h5>
                <Form.Control type="text" placeholder="Enter your name" onChange={(e) => updateSignupInfo({...signupInfo, name: e.target.value})}/>
                <h5>Password</h5>
                <Form.Control type="password" placeholder="Enter your password" onChange={(e) => updateSignupInfo({...signupInfo, password: e.target.value})}/>
                <Button className="formButton" type="submit">
                    {isSignupLoading ? "Loading..." : "Sign up!"}
                </Button>
            </Stack>
          </Col>
          
        </Row>
      </Form>
    </>
  );
};

export default Signup;
