import { Routes, Route, Navigate } from "react-router-dom";
import Chat from "./pages/chat";
import Signup from "./pages/signup";
import Login from "./pages/login";
import ChatRoom from "./pages/chatroom";

import "bootstrap/dist/css/bootstrap.min.css";
import "./assets/base.css";
import { Container, Nav } from "react-bootstrap";
import NavBar from "./components/NavBar";
import { useContext } from "react";
import { AuthContext } from "./context/AuthContext";
import { ChatContextProvider } from "./context/ChatContext";

function App() {
  const {user} = useContext(AuthContext);
  return (
    <ChatContextProvider user = {user}>
      <NavBar />
      <Container>
        <Routes>
          <Route path="/" element={user ? <Chat /> : <Login/>} />
          <Route path="/signup" element={user ? <Chat /> : <Signup/>} />
          <Route path="/login" element={user ? <Chat /> : <Login/>} />
          <Route path="/chatroom" element={user ? <ChatRoom /> : <Login/>} />
          <Route path="*" element={<Navigate to="/" />} />
        </Routes>
      </Container>
    </ChatContextProvider>
  );
}

export default App;
