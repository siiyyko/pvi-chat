const { Server } = require("socket.io");

const io = new Server({ cors: "http://localhost:5173" });

let onlineUsers = [];
let chatRoomUsers = [];

io.on("connection", (socket) => {
  console.log("new connection", socket.id);

  //listen to a connection
  socket.on("addNewUser", (userId) => {
    !onlineUsers.some((user) => user.userId === userId) &&
      onlineUsers.push({
        userId,
        socketId: socket.id,
      });
  io.emit("getOnlineUsers", onlineUsers);

  });
  console.log("ONLINE USERS", onlineUsers);

  //add message
  socket.on("sendMessage", (message) => {
    const user = onlineUsers.find(user => user.userId === message.recipientId)

    if(user){
        io.to(user.socketId).emit("getMessage", message);
        io.to(user.socketId).emit("getNotification", {
          senderId: message.senderId,
          isRead: false,
          date: new Date()
        });
    }
  })

  socket.on("disconnect", () => {
    onlineUsers = onlineUsers.filter(user => user.socketId !== socket.id);
    io.emit("getOnlineUsers", onlineUsers);
  })

  socket.on('joinRoom', ({ roomId, userId }) => {
    socket.join(roomId);
    console.log(`User ${userId} joined room ${roomId}`);
    !chatRoomUsers.some((user) => user.userId === userId) &&
      chatRoomUsers.push({
        userId,
        socketId: socket.id,
        chatRoomId: roomId
      });
  io.to(roomId).emit("getChatRoomUsers", chatRoomUsers);
  });

  socket.on('sendChatRoomMessage', ({roomId, textMessage, userId}) => {
    console.log("MESSAGE: ", textMessage)
    io.to(roomId).emit('receiveMessage', {textMessage, userId});
  })

});

const port = 3000;

io.listen(port);
